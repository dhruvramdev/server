var express = require('express');

app = express();

var bodyParser = require('body-parser');  
var morgan = require('morgan');  
var passport = require('passport');  
var jwt = require('jsonwebtoken');  
var mongoose = require('mongoose');  

var config = require('./config/main');  

mongoose.connect(config.database);  

app.use(bodyParser.urlencoded({ extended: false }));  
app.use(bodyParser.json());

// Log requests to console
app.use(morgan('dev'));

app.use(passport.initialize());  

// Bring in defined Passport Strategy

app.use('/api/auth/', require('./routes/auth'));

// Home route. We'll end up changing this to our main front end index later.
app.get('/', function(req, res) {  
  res.send('Relax. We will put the home page here later.');
});

app.get('/dashboard', passport.authenticate('jwt', { session: false }), function(req, res) {  
  res.send('It worked! User id is: ' + req.user._id + '.');
});

var port = 3000 ;

app.listen(port);
console.log('The magic happens on port ' + port);